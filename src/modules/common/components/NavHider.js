import React from 'react';
import { Route, Switch } from 'react-router-dom'
import _ from 'lodash'
class Component extends React.Component {
  render() {
    return (
      <Switch>
        {
          _.map(this.props.routes, route =>{
            return <Route key={route} path={route} />
          })
        }
        <Route>
          {this.props.children}
        </Route>
      </Switch>
    )
  }
}
export default Component
