import _ from 'lodash'
const required = (val) => val && _.trim(val).length;
const minLength = (len) => (val) => val !== undefined && val.length >= len;
const maxLength = (len) => (val) => val !== undefined && val.length <= len;
const containsNumber = (val) => val && /\d/.test(val)
const containsLetter = (val) => val && /[a-z]/i.test(val)
const onlyAlpha = (val) => val !== undefined && /^\w+$/.test(val)
const isEmail = (val) => val !== undefined && /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(val)
export {
  required,
  minLength,
  maxLength,
  containsNumber,
  containsLetter,
  onlyAlpha,
  isEmail,
}
