import React from 'react';
import {Button} from 'react-bootstrap'
import { Control } from 'react-redux-form';
import FontAwesomeIcon from '@fortawesome/react-fontawesome'
import Spinner from '../components/Spinner'

const SubmitButton = ({model, label, pending, className}) => {
  if(!className){
    className = ''
  }
  if(!label){
    label = 'Save'
  }
  let disabled = {valid: false} || pending // NOTE: if pending then always disabled, otherwise based on form validation
  return (<Control className={className + ' btn-primary'} type="submit" model={model} getValue={(event, props)=>{return props.modelValue}} component={Button} disabled={disabled}>
    <span>{label}</span>
    {!pending && <FontAwesomeIcon className="icon-after" icon={['fal', 'arrow-right']}/>}
    {pending && <Spinner className="spinner" dark={true}/>}
  </Control>)
}
export default SubmitButton
