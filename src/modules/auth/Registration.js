import React from 'react';
import {Link} from 'react-router-dom'
import {authRequirements} from '../common/constants'
import Title from '../common/components/Title'
import Input from '../common/hookforms/Input'
import Email from '../common/hookforms/Email'
import Submit from '../common/hookforms/Submit'
import Password from '../common/hookforms/Password'
import CenteredScreen from '../layout/CenteredScreen'
import {Form} from 'react-bootstrap'
import useForm from 'react-hook-form'
import styled from 'styled-components'
import AuthBanner from './AuthBanner'
const Wrapper = styled(CenteredScreen)`
  .auth-banner{
    width:325px;
    margin-bottom:15px;
    @media (min-width: 768px) {
        width:400px;
    }
  }
  form{
    width:300px;
    text-align:left;
    margin:0 auto 15px;
    .submit{
      margin-top:42px;
      text-align:center;
    }
  }
`
const Registration = ({location, history})=> {
  const form = useForm()
  const onSubmit = (val) =>{}
  const onlyAlpha = /^\w+$/
  return (<Wrapper className="screen-login">
    <Title title="Log in"/>
    <div className="container-fluid">
      <AuthBanner/>
      <Form horizontal onSubmit={form.handleSubmit(onSubmit)}>
        <Input name="username" form={form} label="Username"
        minLength={authRequirements.USERNAME_MIN} maxLength={authRequirements.USERNAME_MAX}
        pattern={onlyAlpha} patternLabel='Must be only a-z 0-9' required/>
        <Email name="email" form={form} label="Email" required/>
        <Password name="password" form={form} label="Password" required/>
        <p>By clicking Create Account you agree to our <Link to="/terms">Terms</Link> and <Link to="/privacy">Privacy Policy</Link></p>
        <Submit className="submit" label="Create account" form={form} primary/>
      </Form>
      <p>Already have an account? <Link to="login">Sign in now</Link></p>
    </div>
  </Wrapper>)
}
export default Registration
