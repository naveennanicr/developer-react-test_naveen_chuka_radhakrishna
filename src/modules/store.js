
import {combineReducers, createStore, applyMiddleware, compose } from "redux";
import thunk from 'redux-thunk'
import appReducer from './reducer'

export function createCognissStore(history){

  const reducers = combineReducers({
    app: appReducer,
  })
  const enhancers = compose(
    applyMiddleware(thunk),
  )
  const store = createStore(reducers, enhancers)
  return store
}
