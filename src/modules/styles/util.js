import Color from 'color'

export const constrastRatio = 4.5

export function contrast(foreground, background){
  let foreGroundLuminosity = Color(foreground).luminosity()
  let backgroundLuminosity = Color(background).luminosity()
  return (foreGroundLuminosity + 0.05) / (backgroundLuminosity + 0.05)
}
