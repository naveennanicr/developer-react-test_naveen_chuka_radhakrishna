import styled from 'styled-components'
import Screen from './Screen'
export default styled(Screen) `
  flex:1 0 auto;
  padding-top:${props => props.fullscreen? '0': '64px'};
  /* min-height:100%; */
  display:flex;
  flex-direction:column;
  background-color:${props => props.theme.colors.offWhite};
  .screen-content{
    flex: 1 0;
    padding: 30px 15px 30px 15px;
  }
  @media (min-width: 1024px) {
    flex-direction:row;
    .screen-content{
      padding: 30px 30px 30px 35px;
      max-width: 1200px;
      margin: 0 auto;
    }
  }
  @media (mid-width: 768px) {
    .screen-content {
      padding: 0 15px 0 15px;
    }
  }
`
