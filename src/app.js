import React, { useState } from "react";
import "./styles.css";
import TermsModal from "./components/TermsModal";

import { BrowserRouter as Router, Link, Route, Switch } from "react-router-dom";

const Register = (props) => {
  const [isAccepted, setAccepted] = useState(false);

  const onCancel = () => {
    // when cancelled go back to login
    props.history.replace("/login");
  };

  const onAccept = () => {
    setAccepted(true);
  };

  // if not accepetd show the terms modal
  if (!isAccepted) {
    return <TermsModal onCancel={onCancel} onAccept={onAccept} />;
  }

  // show login
  return <div>welcome to register</div>;
};

export default function App() {
  return (
    <Router>
      <div className="App">
        <Link to="register">Create account</Link>
        <Switch>
          <Route path="/register" component={Register} />
        </Switch>
      </div>
    </Router>
  );
}
